booth (1.2-2) unstable; urgency=medium

  [ Athos Ribeiro ]
  * d/control: Replace B-D on gcrypt for gnutls (Closes: #1090830)
  * d/rules: explicitly link against gnutls

 -- Valentin Vidic <vvidic@debian.org>  Sun, 26 Jan 2025 16:12:57 +0100

booth (1.2-1) unstable; urgency=medium

  * New upstream version 1.2
    Includes fix for CVE-2024-3049 (Closes: #1073249)
  * d/patches: refresh for new version

 -- Valentin Vidic <vvidic@debian.org>  Sat, 28 Sep 2024 20:29:08 +0200

booth (1.1-4) unstable; urgency=medium

  * d/tests: increase timeouts for slow environments

 -- Valentin Vidic <vvidic@debian.org>  Tue, 16 Jul 2024 22:06:03 +0200

booth (1.1-3) unstable; urgency=medium

  * d/tests: improve handling of slow environments
  * d/patches: add fix for python invalid escape sequence warnings

 -- Valentin Vidic <vvidic@debian.org>  Mon, 15 Jul 2024 20:50:16 +0200

booth (1.1-2) unstable; urgency=medium

  * d/patches: add fix for CVE-2024-3049 (Closes: #1073249)
  * d/install: move systemd files to /usr/lib (Closes: #1073718)
  * d/control: update Standards-Version to 4.7.0
  * d/control: update Build-Depends for pkgconf
  * d/lintian-overrides: cleanup unused tags

 -- Valentin Vidic <vvidic@debian.org>  Sun, 14 Jul 2024 23:29:30 +0200

booth (1.1-1) unstable; urgency=medium

  * New upstream version 1.1

 -- Valentin Vidic <vvidic@debian.org>  Sun, 22 Oct 2023 23:21:48 +0200

booth (1.0-283-g9d4029a-3) unstable; urgency=medium

  * d/clean: remove generated .version file (Closes: #1043927)

 -- Valentin Vidic <vvidic@debian.org>  Sat, 19 Aug 2023 23:37:36 +0200

booth (1.0-283-g9d4029a-2) unstable; urgency=medium

  * d/install: place files in /lib/systemd/system (Closes: #1034211)

 -- Valentin Vidic <vvidic@debian.org>  Wed, 12 Apr 2023 22:58:53 +0200

booth (1.0-283-g9d4029a-1) unstable; urgency=medium

  * New upstream version 1.0-283-g9d4029a
  * d/control: drop lsb-base dependency
  * d/control: update Standards-Version to 4.6.2

 -- Valentin Vidic <vvidic@debian.org>  Sun, 05 Mar 2023 22:26:42 +0100

booth (1.0-268-gdce51f9-1) unstable; urgency=medium

  * New upstream version 1.0-268-gdce51f9
    Includes fix for CVE-2022-2553
  * d/control: update Standards-Version to 4.6.1
  * d/lintian-overrides: update for new lintian
  * d/tests: test if bad key fails authentication

 -- Valentin Vidic <vvidic@debian.org>  Sat, 30 Jul 2022 19:11:58 +0200

booth (1.0-253-g95d854b-1) unstable; urgency=medium

  * New upstream version 1.0-253-g95d854b
  * d/install: include pkgconfig file for booth
  * d/copyright: update copyright for debian files
  * d/tests: require new crmsh for python 3.10 (Closes: #1009087)
  * d/watch: update github url

 -- Valentin Vidic <vvidic@debian.org>  Sun, 24 Apr 2022 11:39:44 +0200

booth (1.0-237-gdd88847-4) unstable; urgency=medium

  * d/install: avoid installing systemd files twice

 -- Valentin Vidic <vvidic@debian.org>  Tue, 28 Sep 2021 22:11:47 +0200

booth (1.0-237-gdd88847-3) unstable; urgency=medium

  * d/tests: wait for cluster startup (Closes: #994794)
  * d/install: place systemd files in /usr/lib/systemd
  * d/control: update Standards-Version to 4.6.0

 -- Valentin Vidic <vvidic@debian.org>  Wed, 22 Sep 2021 22:57:11 +0200

booth (1.0-237-gdd88847-2) unstable; urgency=medium

  * d/watch: update version to 4
  * d/control: update Standards-Version to 4.5.1
  * d/tests: additional loging to debug corosync start

 -- Valentin Vidic <vvidic@debian.org>  Tue, 26 Jan 2021 22:09:19 +0100

booth (1.0-237-gdd88847-1) unstable; urgency=medium

  * New upstream version 1.0-237-gdd88847
  * d/rules: dh_missing now defaults to --fail-missing
  * d/source/lintian-overrides: update for new version
  * d/rules: install firewalld service config
  * d/patches: cleanup for new version
  * d/tests: unit tests refuse to run as root
  * d/tests: run additional sites for boothd test

 -- Valentin Vidic <vvidic@debian.org>  Mon, 02 Nov 2020 13:07:48 +0100

booth (1.0-205-gc9e90fe-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Valentin Vidic ]
  * d/lintian-overrides: update for new lintian
  * d/upstream/metadata: add repository info
  * d/tests: add timeout for boothd test (Closes: #973359)

 -- Valentin Vidic <vvidic@debian.org>  Sun, 01 Nov 2020 13:40:15 +0100

booth (1.0-205-gc9e90fe-1) unstable; urgency=medium

  * New upstream version 1.0-205-gc9e90fe
  * d/patches: refresh for new version
  * d/control: update build depends on pacemaker-dev (Closes: #963682)
  * d/control: add libdbus-1-dev build-depends missing from pacemaker-dev
  * d/control: use debhelper 13
  * d/control: update Maintainer mailing list address
  * d/lintian-overrides: update for new version
  * d/rules: fix test building

 -- Valentin Vidic <vvidic@debian.org>  Sat, 27 Jun 2020 12:18:48 +0200

booth (1.0-174-gce9f821-2) unstable; urgency=medium

  * d/patches: try to fix flaky lock file test
  * d/tests: try to store unit test logs
  * d/patches: add fix for gcc-10 build (Closes: #957051)
  * d/control: update Standards-Version to 4.5.0
  * d/lintian-overrides: ignore init script warning
  * d/lintian-overrides: ignore pedantic warnings
  * d/rules: disable init script by default

 -- Valentin Vidic <vvidic@debian.org>  Sat, 18 Apr 2020 14:23:20 +0200

booth (1.0-174-gce9f821-1) unstable; urgency=medium

  * New upstream version 1.0-174-gce9f821
  * d/patches: use pkgconfig to configure libxml-2.0 (Closes: #949057)
  * d/patches: refresh for new version
  * d/control: use debhelper 12
  * d/control: use Rules-Requires-Root: no
  * d/control: update Standards-Version to 4.4.1
  * d/TODO: rename package TODO file
  * d/salsa-ci.yml: enable Debian CI

 -- Valentin Vidic <vvidic@debian.org>  Sun, 19 Jan 2020 03:59:41 +0100

booth (1.0-162-g27f917f-2) unstable; urgency=medium

  * d/tests: ignore Blind faith warning from pacemaker

 -- Valentin Vidic <vvidic@debian.org>  Sun, 20 Jan 2019 11:43:08 +0100

booth (1.0-162-g27f917f-1) unstable; urgency=medium

  * New upstream version 1.0-162-g27f917f
  * d/control: update my email in Uploaders
  * d/patches: refresh for new version
  * d/control: use asciidoctor instead of asciidoc (Closes: #895485)
  * d/tests: use python3 to run unit tests
  * d/patches: fix cleanup of service.in files
  * d/control: update Standards-Version to 4.2.1

 -- Valentin Vidic <vvidic@debian.org>  Thu, 11 Oct 2018 10:14:08 +0200

booth (1.0-7) unstable; urgency=medium

  * d/control: update Vcs URLs to use salsa
  * d/patches: add fix for unit tests in Ubuntu (Closes: #895179)

 -- Valentin Vidic <Valentin.Vidic@CARNet.hr>  Mon, 09 Apr 2018 20:06:49 +0200

booth (1.0-6) unstable; urgency=medium

  * d/rules: replace dh_systemd_{enable,start} with dh_installsystemd
  * d/tests: run upstream unit tests (Closes: #888730)
  * d/copyright: update Format URL
  * d/copyright: remove duplicate paragraph
  * d/control: remove duplicate Section
  * d/patches: fix spelling errors reported by lintian
  * d/rules: don't ship html versions of man pages
  * d/booth.service: add Documentation key

 -- Valentin Vidic <Valentin.Vidic@CARNet.hr>  Mon, 29 Jan 2018 21:32:49 +0100

booth (1.0-5) unstable; urgency=medium

  * d/control: add cluster-glue and psmisc to BD

 -- Valentin Vidic <Valentin.Vidic@CARNet.hr>  Thu, 25 Jan 2018 21:20:47 +0100

booth (1.0-4) unstable; urgency=medium

  * d/control: replace deprecated priority extra
  * d/control: update Standards-Version to 4.1.3
  * d/compat: update to debhelper v11
  * d/patches: add fix for failing unit tests

 -- Valentin Vidic <Valentin.Vidic@CARNet.hr>  Thu, 04 Jan 2018 13:10:21 +0100

booth (1.0-3) unstable; urgency=medium

  * Fix GCC-7 warning on buffer size (Closes: #853337)
  * Ignore quilt working directory (.pc)
  * d/control: move lsb-base depends to booth package
  * d/compat: update debhelper version to 10
  * d/control: update standards version to 4.0.0
  * d/control: remove ${shlibs:Depends} from boot-pacemaker
  * d/rules: use dh_missing --fail-missing
  * d/contro: add myself to Uploaders
  * d/rules: autoreconf and systemd default in dh 10
  * d/rules: make generated documentation reproducible
  * d/tests: test booth service and pacemaker resource
  * d/control: remove useless build-depends
  * d/control: set booth architecture to linux-any
  * d/control: set booth-pacemaker architecture to all
  * d/tests: add wait after booth service restart

 -- Valentin Vidic <Valentin.Vidic@CARNet.hr>  Wed, 02 Aug 2017 18:31:21 +0200

booth (1.0-2) unstable; urgency=medium

  [ Christoph Berg ]
  * Update Vcs URLs.
  * Drop obsolete B-D on help2man.

  [ Adrian Vondendriesch ]
  * Add lsb-base to booth depencencies.
  * Bump standards version.

 -- Adrian Vondendriesch <adrian.vondendriesch@credativ.de>  Wed, 12 Oct 2016 21:59:58 +0200

booth (1.0-1) unstable; urgency=low

  [ Adrian Vondendriesch ]
  * New upstream release (1.0)
  * Add watch file.
  * Bump standards version.
  * Ship our own .service files to make use of systemd-generators.
  * Symlink ../man8.booth.8.gz to ../man8.boothd.8.gz

  [ Christoph Berg ]
  * Update package descriptions. Closes: #819575.
  * Architecture: any.
  * Fix init script to work with boothd's pid file format.

 -- Adrian Vondendriesch <adrian.vondendriesch@credativ.de>  Mon, 28 Mar 2016 18:14:21 +0200

booth (0.2.0-1) UNRELEASED; urgency=low

  * Non-maintainer upload.
  * New upstream release (Closes: #777806)

 -- Oscar Salvador Vilardaga <osalvador.vilardaga@gmail.com>  Mon, 20 Jul 2015 16:30:18 +0200

booth (0.1.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "FTBFS on some arches due to -Werror=cast-align"
    Patch by Konstantinos Margaritis
    Closes: #691574

 -- Anibal Monsalve Salazar <anibal@debian.org>  Sat, 15 Feb 2014 12:25:41 +0000

booth (0.1.0-1) unstable; urgency=low

  * Initial release. (Closes: #674284)

 -- Martin Loschwitz <madkiss@debian.org>  Sun, 27 May 2012 18:02:21 +0000
